﻿using UnityEngine;
using System.Collections;

public class DropBad : DropBase {

	private GameObject m_player;




	// Use this for initialization
	void Start () {
		m_player = GameObject.FindWithTag ("Player");
	}
	
	// Update is called once per frame
	void Update () {

		BaseUpdate ();

		transform.Rotate (new Vector3 (-Time.deltaTime*500.0f, 0, 0), Space.Self);
	}

	void OnTriggerEnter(Collider col)
	{

		if (col.gameObject.tag == "Tentacle") 
		{
			m_player.GetComponent<Octopus>().CutTentacle(col.gameObject);
			GetComponent<CapsuleCollider>().enabled = false;
		}
	}

}
