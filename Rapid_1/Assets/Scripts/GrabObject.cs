﻿using UnityEngine;
using System.Collections;

public class GrabObject : MonoBehaviour {

	public float m_fCoolRate;
	private float m_fHotRate;

	private float m_fLastStateChange = 0.0f;

	private bool m_bCold = true;

	// Use this for initialization
	void Start () {
		m_fHotRate = Random.Range (8.0f, 15.0f);
	}
	
	// Update is called once per frame
	void Update () {
		m_fLastStateChange += Time.deltaTime;
		if (m_bCold) {
			if (m_fLastStateChange > m_fHotRate) {
				m_fLastStateChange = 0.0f;
				m_bCold = false;
				GetComponent<MeshRenderer>().enabled = false;
			}
		} else {
			if(m_fLastStateChange > m_fCoolRate)
			{
				m_fLastStateChange = 0.0f;
				m_bCold = true;
				GetComponent<MeshRenderer>().enabled = true;
			}
		}
	}

	public bool IsCold()
	{
		return m_bCold;
	}
	
}
