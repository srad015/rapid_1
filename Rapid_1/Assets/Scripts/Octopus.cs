﻿#undef UNITY_WP8

using UnityEngine;
using System.Collections;

public class Octopus : MonoBehaviour {
	public GameManager m_gameManager;
	public GameObject m_tentaclePrefab;
	public int  m_iTentacleCount;


	private SpringJoint[] m_springJoints;
	private GameObject[]  m_tentacles;
	private bool[]	      m_aliveTentacles;
	private bool[]		  m_retractedTentacles;

	private GameObject[] m_grabObjects;

	private int m_iAliveTentacles;
	private int m_iRetractedTentacles;

	private bool m_bDead = false;
	private bool m_bDeathComplete = false;

	private float m_fLastTap = 0.0f;
	void Awake()
	{

		m_tentacles = new GameObject[m_iTentacleCount];
		m_aliveTentacles = new bool[m_iTentacleCount];
		m_retractedTentacles  = new bool[m_iTentacleCount];
			
		for (int i = 0; i < m_iTentacleCount; ++i) {
			//Create component
			gameObject.AddComponent<SpringJoint> ();
			
			//Create tentacles
			GameObject _tentacleObj = (GameObject)Instantiate(m_tentaclePrefab,transform.position,transform.rotation);
			_tentacleObj.transform.parent = transform;
			m_tentacles[i] = _tentacleObj;
		}
		
		
		
		//Grab components
		m_springJoints = gameObject.GetComponents<SpringJoint> ();
		
		for (int i = 0; i < m_iTentacleCount; ++i) {
			//Joints
			m_springJoints [i].autoConfigureConnectedAnchor = false;
			m_springJoints [i].anchor = new Vector3(0,0,0);
			//killTentacle(i);
			m_aliveTentacles[i] = true;
			m_retractedTentacles[i] = true;
			m_iRetractedTentacles = m_iTentacleCount;
		}

			
	}

	// Use this for initialization
	void Start () 	{
		//m_lineTargetPositions = new Vector3[m_iTentacleCount];

		m_iAliveTentacles = m_iTentacleCount;
		m_grabObjects = GameObject.FindGameObjectsWithTag ("GrabObject");

		for (int i = 0; i < m_iTentacleCount; ++i)
			ExtendTentacle (i);
		
	}
	
	// Update is called once per frame
	void Update () 	
	{

		if (m_bDead ) {
			transform.Translate (Vector3.forward, Space.World);

			if(!m_bDeathComplete)
			{
				for (int i = 0; i < m_iTentacleCount; ++i)
					m_springJoints[i].breakForce = 0.0000001f;
				m_bDeathComplete = true;
			}

			return;
		}


		updateTentacles();
		int _iInputIndex = -1;
	
#if UNITY_WP8
		m_fLastTap += Time.deltaTime;
		if (Input.touchCount > 0 && m_fLastTap > 0.2f) {
			m_fLastTap = 0.0f;
			Touch myTouch = Input.touches[0];
			RaycastHit _hit;
			Ray _ray = Camera.main.ScreenPointToRay (myTouch.position);
			//Ray _ray = Camera.main.ScreenPointToRay (Input.mousePosition);
			if(Physics.Raycast(_ray, out _hit)){
				Debug.Log (_iInputIndex);
				if(_hit.transform.gameObject.tag == "GrabObject")
				{

					_iInputIndex = m_gameManager.GetGrabObjectID(_hit.transform.gameObject);
				}
			}
		}
#else


		if (Input.GetKeyDown (KeyCode.Alpha1)) {
			_iInputIndex = 0;
		} else if (Input.GetKeyDown (KeyCode.Alpha2)) {
			_iInputIndex = 1;
		}else if (Input.GetKeyDown (KeyCode.Alpha3)) {
			_iInputIndex = 2;
		}else if (Input.GetKeyDown (KeyCode.Alpha4)) {
			_iInputIndex = 3;
		}else if (Input.GetKeyDown (KeyCode.Alpha5)) {
			_iInputIndex = 4;
		}else if (Input.GetKeyDown (KeyCode.Alpha6)) {
			_iInputIndex = 5;
		}else if (Input.GetKeyDown (KeyCode.Alpha7)) {
			_iInputIndex = 6;
		}else if (Input.GetKeyDown (KeyCode.Alpha8)) {
			_iInputIndex = 7;
		}
#endif

		if (_iInputIndex >= m_iTentacleCount || _iInputIndex < 0)
			return;
		if (!m_aliveTentacles [_iInputIndex])
			return;

		if(m_retractedTentacles[_iInputIndex])
			HandleInputTentacleExtend (_iInputIndex);
		else
			HandleInputTentacleRetract (_iInputIndex);




	}
	void HandleInputTentacleRetract(int _iInputIndex)
	{

		RetractTentacle(_iInputIndex);		

		
	}
	void HandleInputTentacleExtend( int _iInputIndex)
	{

		//Check to see if the grab object is hot or cold
		if (!m_gameManager.GrabObjectIsCold (_iInputIndex))
			return;



		ExtendTentacle(_iInputIndex);
						
		//RaycastHit _hit;
		//Ray _ray = Camera.main.ScreenPointToRay (Input.mousePosition);
		//if(Physics.Raycast(_ray, out _hit)){
		//}

	}

	private void killTentacle(int _iIndex)
	{

		m_iAliveTentacles--;
		m_aliveTentacles [_iIndex] = false;

		if (m_iAliveTentacles == 0)
			m_bDead = true;
		else
			m_gameManager.RestartTentacleTimer ();
	}
	
	private void RetractTentacle(int _iIndex)
	{
		m_iRetractedTentacles++;
		m_retractedTentacles [_iIndex] = true;

		if (m_iRetractedTentacles == m_iTentacleCount) {
			m_bDead = true;
			m_gameManager.SetPusName(0);
		}
			
	}

	private void ExtendTentacle(int _iIndex)
	{

		m_springJoints[_iIndex].connectedAnchor = m_grabObjects[_iIndex].transform.position;
		m_retractedTentacles [_iIndex] = false;

		m_iRetractedTentacles--;


	}

	void updateTentacles()
	{
		for (int i = 0; i < m_iTentacleCount; ++i) {
			{

				if(!m_aliveTentacles[i] || m_retractedTentacles[i])
					m_springJoints [i].connectedAnchor = transform.position;

				//Retract extended tentacles on hot grab objects
				if(m_aliveTentacles[i] && !m_retractedTentacles[i] && !m_gameManager.GrabObjectIsCold (i))
					RetractTentacle (i);
			}





			// Get the length of the tentacle.
			Vector3 tentacleLine = m_springJoints[i].connectedAnchor - transform.position;
			float tentacleLength = tentacleLine.magnitude * 0.65f;
			
			Vector3 tentaclePosition = m_springJoints[i].connectedAnchor + transform.position;
			tentaclePosition /= 2;
			
			
			m_tentacles[i].transform.position = tentaclePosition;
			
			// Scale the tentacle to it's length.
			Vector3 scale = m_tentacles[i].transform.localScale;
			scale.y = tentacleLength;
			m_tentacles[i].transform.localScale = scale;
			
			float _fX = 1.0f;
			if(tentacleLine.x > 0) {
				_fX = -1.0f;
			}
			else {
				
			}
			
			float angle = Vector3.Angle(tentacleLine, new Vector3(0, _fX, 0));
			
			
			// Set the rotation of tentacle to the line's rotation.
			m_tentacles[i].transform.rotation = Quaternion.AngleAxis(angle, new Vector3(0, 0, 1));
			//m_tentacles[i].transform.rotation = Quaternion.LookRotation(-tentacleLine);
			//m_lineRenderers [i].SetPosition (0, transform.position);
			//m_lineRenderers [i].SetPosition (1, m_lineTargetPositions[i]);
		}
	}

	public void CutTentacle(GameObject _tentacle)
	{
		for (int i = 0; i < m_iTentacleCount; ++i) {
			if(m_tentacles[i].Equals (_tentacle))
			{

				m_grabObjects[i].GetComponentInChildren<TextMesh>().color = Color.red;
				killTentacle(i);
				m_gameManager.SetPusName(m_iAliveTentacles);
				Debug.Log (m_iTentacleCount);
				return;
			}
		}

	}

	public int AliveTentacleCount(){
		return m_iAliveTentacles;
	}
	public bool MissingTentacles()
	{
	
		return m_iAliveTentacles != m_iTentacleCount;
	}
	public int RegenerateTentacle(){
		if (m_iAliveTentacles == m_iTentacleCount || m_iAliveTentacles == 0)
			return -1;

		for (int i = 0; i < m_iTentacleCount; ++i) {
			if(!m_aliveTentacles[i])
			{
				m_iAliveTentacles++;
				m_grabObjects[i].GetComponentInChildren<TextMesh>().color = Color.blue;
				m_aliveTentacles[i] = true;
				RetractTentacle(i);
				return i;

			}	

		}
		return -1;
	}
}
