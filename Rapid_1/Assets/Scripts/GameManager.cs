using UnityEngine;
using System.Collections;

public class GameManager : MonoBehaviour {

	public int 		m_iBadDropCount;
	//public int 		m_iGoodDropCount;
	public int		m_iGrabObjectCount;
	public float    m_fDropRate;
	public float    m_fTentacleRegenTime;

	public GameObject m_dropBadPrefab;
	public GameObject m_dropBadPrefab1;
	public GameObject m_dropBadPrefab2;
	//public GameObject m_dropGoodPrefab;
	public GameObject m_grabPrefab;

	private float m_fLastBadDrop = 0.0f;
	//private float m_fLastGoodDrop = 0.0f;

	private DropBad[]  m_badDropScripts;
	//private DropGood[] m_goodDropScripts;
	private GameObject[] m_grabObjects;

	private string[] m_pusNames;

	private TextMesh m_textMeshComponent;
	private TextMesh m_timerTextMeshComponent;

	private int m_iTargetFontSize;

	private float m_fTentacleRegenTimer;

	void awake()
	{



	
	}

	public void SetPusName(int _iIndex){
		if (_iIndex < 0 || _iIndex > 8)
			return;
	
		m_textMeshComponent.text = m_pusNames [_iIndex];

		m_textMeshComponent.fontSize = 0;

	}

	// Use this for initialization
	void Start () {


		m_pusNames = new string[9];
		m_pusNames [0] = "Deadopus";
		m_pusNames [1] = "Unopus";
		m_pusNames [2] = "Duopus";
		m_pusNames [3] = "Triceptopus";
		m_pusNames [4] = "Quadopus";
		m_pusNames [5] = "Pentopus";
		m_pusNames [6] = "Centopus";
		m_pusNames [7] = "Septopus";
		m_pusNames [8] = "Octopus";
		m_textMeshComponent = GetComponent<TextMesh> () as TextMesh;

		m_textMeshComponent.text = m_pusNames [8];

		m_iTargetFontSize = m_textMeshComponent.fontSize;

		//for(int i = 0 ; i < m_iGoodDropCount; ++i)
		//	Instantiate (m_dropGoodPrefab, new Vector3 (0, 0, -20), Quaternion.identity);

		for(int i = 0 ; i < m_iBadDropCount; ++i)
			Instantiate (m_dropBadPrefab, new Vector3 (0, 0, -20), Quaternion.identity);
		for(int i = 0 ; i < m_iBadDropCount; ++i)
			Instantiate (m_dropBadPrefab1, new Vector3 (0, 0, -20), Quaternion.identity);
		for(int i = 0 ; i < m_iBadDropCount; ++i)
			Instantiate (m_dropBadPrefab2, new Vector3 (0, 0, -20), Quaternion.identity);
		
		for (int i = 0; i < m_iGrabObjectCount; ++i)
			Instantiate (m_grabPrefab, new Vector3 (-7.7f, -7.7f, 0.0f), Quaternion.identity);


		GameObject[] _badDropGameObjects = GameObject.FindGameObjectsWithTag ("DropBad");
		m_badDropScripts = new DropBad[m_iBadDropCount*3];
		for (int i =0; i < m_iBadDropCount*3; ++i)
			m_badDropScripts [i] = _badDropGameObjects [i].GetComponent<DropBad> ();



		//GameObject[] _goodDropGameObjects = GameObject.FindGameObjectsWithTag ("DropGood");
		//m_goodDropScripts = new DropGood[m_iGoodDropCount];
		//for (int i = 0; i < m_iGoodDropCount; ++i)
		//	m_goodDropScripts [i] = _goodDropGameObjects [i].GetComponent<DropGood> ();

		m_grabObjects = GameObject.FindGameObjectsWithTag ("GrabObject");
		for (int i = 0; i < m_iGrabObjectCount; ++i) {
			m_grabObjects [i].transform.RotateAround (new Vector3 (0, 0, 0), new Vector3 (0, 0, 1), -(i)*45);

			TextMesh _textMesh = m_grabObjects[i].GetComponentInChildren<TextMesh>();
			_textMesh.text = (i+1).ToString ();
			_textMesh.transform.rotation = Quaternion.identity;
		}

		m_timerTextMeshComponent = GameObject.FindGameObjectWithTag("TentacleTimer").GetComponent<TextMesh> () as TextMesh;
		m_timerTextMeshComponent.text = "0";

	}

	public int GetGrabObjectID(GameObject _grabObj){
		for (int i = 0; i < m_iGrabObjectCount; ++i) {
			if(m_grabObjects[i].Equals(_grabObj))
				return i;
		}
		return -1;
	}

	public bool GrabObjectIsCold(int _iID){
		return m_grabObjects [_iID].GetComponent<GrabObject> ().IsCold ();
	}

	// Update is called once per frame
	void Update () {
	
		UpdateDrops ();

		if (m_textMeshComponent.fontSize < m_iTargetFontSize) {
			m_textMeshComponent.fontSize+= 2;
		}

		if (m_fTentacleRegenTimer > 0) {
			m_fTentacleRegenTimer -= Time.deltaTime;
			//Tell player to regenerate when this ticks to 0
			if(m_fTentacleRegenTimer <= 0)
			{
				GameObject.FindGameObjectWithTag ("Player").GetComponent<Octopus>().RegenerateTentacle();
				m_textMeshComponent.text = m_pusNames[GameObject.FindGameObjectWithTag ("Player").GetComponent<Octopus>().AliveTentacleCount()];
			}
				

		} else {
			if(GameObject.FindGameObjectWithTag ("Player").GetComponent<Octopus>().MissingTentacles())
				m_fTentacleRegenTimer = m_fTentacleRegenTime;
			else
				m_fTentacleRegenTimer = 0;
		}
		m_timerTextMeshComponent.text = m_fTentacleRegenTimer.ToString();
	}

	public void RestartTentacleTimer(){
		m_fTentacleRegenTimer = m_fTentacleRegenTime;
	}

	void UpdateDrops()
	{
		m_fLastBadDrop += Time.deltaTime;
		
		if (m_fLastBadDrop  > m_fDropRate) 
		{
			m_fLastBadDrop = 0.0f;
			
			for (int i =0; i < m_iBadDropCount*3; ++i)
			{
				if(!m_badDropScripts[i].Dropping ())
				{
					m_badDropScripts[i].Drop ();
					break;
				}
			}
		}
		
		//m_fLastGoodDrop += Time.deltaTime;
		
		//if (m_fLastGoodDrop  > m_fDropRate) 
		//{
		//	m_fLastGoodDrop = 0.0f;
			
		//	for (int i =0; i < m_iGoodDropCount; ++i)
		//	{
		//		if(!m_goodDropScripts[i].Dropping ())
		//		{
		//			m_goodDropScripts[i].Drop ();
		//			break;
		///		}
		//	}
		//}

	}
}
