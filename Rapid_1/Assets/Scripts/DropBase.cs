﻿using UnityEngine;
using System.Collections;

public class DropBase : MonoBehaviour {

	public Vector3 m_idlePos; 
	public float   m_fBottomZ; 
	public float   m_fTopZ;
	public float   m_fLeftMax;
	public float   m_fRightMax;

	protected bool   m_bDropping = true;
	
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	protected void BaseUpdate()
	{
		if (m_bDropping) {
			if (transform.position.z > m_fBottomZ) {
				m_bDropping = false;
				transform.position = m_idlePos;
			} else {
				transform.Translate (new Vector3 (0, 0, Time.deltaTime * 5.0f), Space.World);
			}	
		} else {
			GetComponent<MeshRenderer> ().enabled = false;
		}

	}

	public void Drop()
	{
		GetComponent<MeshRenderer> ().enabled = true;
		GetComponent<CapsuleCollider>().enabled = true;
		//Debug.Log ("Dropped");
		m_bDropping = true;
		transform.position = new Vector3 (Random.Range (m_fLeftMax, m_fRightMax),0, m_fTopZ);
	}
	
	public bool Dropping()
	{
		return m_bDropping;
	}
}
